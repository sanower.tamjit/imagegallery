FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} genex-1.0.1.jar
ENTRYPOINT ["java","-jar","/genex-1.0.1.jar"]
